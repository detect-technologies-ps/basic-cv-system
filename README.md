The project 'Basic CV System' is aimed to read video files from a folder and perform batch operations on all files which are: 

1. Rotate frame by 90 degrees
2. Convert the frame to Grayscale
3. Save output a a new video

The frames per second for every video are logged on every output video on the op-left corner.

**Environment setup:**
The project uses Python 3.8 for execution with the following Python libraries needed to be installed:
opencv-python==4.5.3.56

To install the above python library, open Command Prompt and enter:
`pip install opencv-python==4.5.3.56`

**Development Setup:**
The IDE used for the project is PyCharm which can be downloaded from:
https://www.jetbrains.com/pycharm/download/
The latest version comes with in-built support for Python 3.8

**video-operations.py**

This file has the entry function vid_operations() that takes as input the path and the name of the desired operation from the BasicCV.py file.
It reads all the video files(.mp4 and .avi) from the given path and sequentially sends the file to the other function perform_operation().
This function first reads the input video file and grabs the frame width and height, and sets the VideoWriter output video dimensions based on the operation.
If we wish to rotate the video, the height and width params of the video are switched. The VideoWriter() function of the OpenCV object is used to create the output video after the desired operation on the input video frame by frame.
Then, we read the frames one by one and calculate the FPS by subtracting the pervios frame captured system time and the current one. 
Operations of rotating, conversion to grayscale and saving are performed, FPS is written on the output frame and the frame is added to the new output video.

**BasicCV.py**

The BasicCV.py is the entry file that we run from the command prompt. It takes as input the arguments "--operations" and "--video_path"
The _--operations_ argument is mandatory and the options include "rotate", "grayscale" and "all".
The _--video_path_ argument defaults to a folder named "input" assuming that the folder contains the required input videos. If the videos are at any other location, then the whole path of the folder containing the videos is provided.

Navigate to the folder containing the file BasicCV.py.

To perform rotation on all videos:
`python BasicCV.py --operations rotate --video_path C:\Users\ABC\DT_PS\input\`

To convert the videos to grayscale:
`python BasicCV.py --operations grayscale --video_path C:\Users\ABC\DT_PS\input\`

To perform all these operations at once:
`python BasicCV.py --operations all --video_path C:\Users\ABC\DT_PS\input\`
