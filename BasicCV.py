from argparse import ArgumentParser
import os
from video_operations import vid_operations


def main():
    parser = ArgumentParser()
    parser.add_argument(
        "--operations",
        choices=["rotate", "grayscale", "all"],
        required=True,
        help="Operation to Perform",
    )
    parser.add_argument(
        "--video_path", default="input/", help="Path to the videos",
    )
    choice = None
    args = parser.parse_args()
    video_path = args.video_path
    operation = args.operations
    if operation in ["rotate", "grayscale", "all"]:
        print(video_path)
        vid_operations(video_path, operation)
    else:
        print("Invalid choice")
        exit()


if __name__ == "__main__":
    main()
