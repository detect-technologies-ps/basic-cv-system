import cv2
import os
import time


def perform_operation(file, operation):
    cap = cv2.VideoCapture(file)
    # Record the time when we processed last frame
    prev_frame_time = 0
    # Record the time at which we processed current frame
    new_frame_time = 0
    print(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print(operation)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    file_name = (operation + "_" + file)
    if operation == "rotate" or operation == "all":
        writer = cv2.VideoWriter(file_name, cv2.VideoWriter_fourcc(*'DIVX'), 20, (height, width))
    else:
        writer = cv2.VideoWriter(file_name, cv2.VideoWriter_fourcc(*'DIVX'), 20, (width, height))
    if not writer:
        print("!!! Failed VideoWriter: invalid parameters")
        exit()
    while True:
        ret, frame = cap.read()
        # time when we finish processing for this frame
        new_frame_time = time.time()
        fps = 1 / (new_frame_time - prev_frame_time)
        prev_frame_time = new_frame_time
        # converting the fps into integer
        fps = str(int(fps))
        # font which we will be using to display FPS
        font = cv2.FONT_HERSHEY_SIMPLEX
        if not ret:
            break
        image = frame.copy()
        if (operation == "grayscale" or operation == "all"):
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if (operation == "rotate" or "all"):
            image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
        # putting the FPS count on the frame
        cv2.putText(image, fps, (7, 70), font, 3, (100, 255, 0), 3, cv2.LINE_AA)
        writer.write(image)
        cv2.imshow('Frame', image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def vid_operations(path, operation):
    for file in os.listdir(path):
        if file.endswith(".mp4") or file.endswith(".avi"):
            perform_operation(file, operation)
